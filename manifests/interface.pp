define network::interface
(
  $static_ip = false,
  $type      = 'Ethernet',
  $onboot    = 'YES',
  $ipaddress = undef,
  $netmask   = undef,
  $gateway   = undef,
){

  if $static_ip {
    $bootproto = 'static'
    if( $ipaddress == undef or $ipaddress == ''){
      fail('Value of ipaddress can\'t be empty')
    }
    if( $netmask == undef or $netmask == ''){
      fail('Value of netmask can\'t be empty')
    }
    if( $gateway == undef or $gateway == ''){
      fail('Value of gateway can\'t be empty')
    }
    file {'/etc/sysconfig/network':
      owner   => 'root',
      group  => 'root',
      content => template("${module_name}/network.erb"),
    } ~>
    service {'network':
      ensure => true,
    }
  }else{
    $bootproto = 'dhcp'
  }

  file {"/etc/sysconfig/network-scripts/ifcfg-${title}":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("${module_name}/ifcfg-eth.erb")
  } #~>
 # exec {"ifdown $title; ifup $title":
#    path        => ['/usr/local/sbin','/usr/local/bin','/usr/sbin','/usr/bin','/bin'],
#    refreshonly => true,
#  }
}
